#include "stm32f042x6.h"

// CN4 pin 15, also: green LED: PB3
// CN3 pin 15 PB4

void delay()
{
	volatile int i;
	for( i=0; i<100000; ++i );
}

int main(void)
{
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;	// GPIO port B clock enable

	GPIOB->MODER   |=  GPIO_MODER_MODER3_0; 	// mode 01: GPIO output
	GPIOB->MODER   &= ~GPIO_MODER_MODER3_1; 	// mode 01: GPIO output
	GPIOB->OTYPER  &= ~GPIO_OTYPER_OT_3; 		// type: push-pull
	GPIOB->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR3;	// speed 00: low speed
	GPIOB->PUPDR   &= ~GPIO_PUPDR_PUPDR3;		// no pull-up, no pull-down

	for(;;) {
		GPIOB->BSRR = GPIO_BSRR_BS_3;			// Bit Set
		delay();
		GPIOB->BSRR = GPIO_BSRR_BR_3;			// Bit Reset
		delay();
	}
}
